///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello C++
///
/// @file hello1.cpp
/// @version 1.0
///
/// Hello world program using namespace
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 04c - Hello C++ - EE 205 - Spr 2021
/// @date   8 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main(){
   cout << "Hello World!" << endl;
   return 0;
}
