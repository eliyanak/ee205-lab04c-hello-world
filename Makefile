###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04c - Hello C++
#
# @file    Makefile
# @version 1.0
#
# @author Eliya Nakamura <eliyanak@hawaii.edu>
# @brief  Lab 04c - Hello C++ - EE 205 - Spr 2021
# @date   8 February 2021
###############################################################################

all: hello1 hello2

hello1: hello1.cpp
	g++ -o hello1 hello1.cpp

hello2: hello2.cpp
	g++ -o hello2 hello2.cpp
