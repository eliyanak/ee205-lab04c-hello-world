///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello C++
///
/// @file hello2.cpp
/// @version 1.0
///
/// Hello world program without using namespace std
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 04c - Hello C++ - EE 205 - Spr 2021
/// @date   8 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(){
   std::cout << "Hello World!" << std::endl;
   return 0;
}
